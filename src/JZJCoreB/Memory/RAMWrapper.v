module RAMWrapper//Wraps the sync_ram module to only respond to accesses to addresses in its address space
(
	input clock,
	
	//Addressing
	input [31:0] address,
	
	//IO
	input [31:0] dataIn,
	output [31:0] dataOut,
	input writeEnable
	
);
/* Parameters */

parameter INITIAL_RAM_CONTENTS = "initialRam.mem";
parameter A_WIDTH = 12;

//Address bounds (wordwise and inclusive)
localparam MIN_RAM_ADDRESS = 32'h00000000;//Starts at byte and word address 0
localparam MAX_RAM_ADDRESS = (2 ** A_WIDTH) - 1;//4096 words by default (16KiB = 16384 bytes total, therefore last address is from 16380 to 16383 (word-wise address 4095))

/* Wires */

wire isInBounds = (address >= MIN_RAM_ADDRESS) && (address <= MAX_RAM_ADDRESS);
wire actualWriteEnable;
wire [31:0] directDataOut;

/* Enable logic */

assign actualWriteEnable = isInBounds ? writeEnable : 1'b0;//only enable write enable if addresses are in ram address space
assign dataOut = isInBounds ? directDataOut : 32'h00000000;//only output data if addresses are in ram address space

/* Sync ram module */

sync_ram #(.FILE(INITIAL_RAM_CONTENTS), .A_WIDTH(A_WIDTH), .D_WIDTH(32), .INITIALIZE_FROM_FILE(1)) ram (.clock(clock), .write_enable(actualWriteEnable), .data_out(directDataOut),
																																		  .read_address(address), .data_in(dataIn), .write_address(address));

endmodule