module PCALU//Updates the pc to the next instruction (either +4 or something else for jumps/branches)
(
	//Output
	output reg [31:0] newPCOut,
	output reg [31:0] rdOut,
	output reg invalidFunct3,
	output jumpMisaligned,
	input outputEnable,//enables both rdOut and newPCOut
	
	//Operation type
	input [1:0] operation,//0 indicates jal, 1 indicates jalr, 2 indicates beq/bne//blt/bge/bltu/bgeu, and 3 indicates increment pc to the next address (pcIn + 4)
	
	//Data sources (registers, immediates and the current program counter value)
	input [31:0] portRS1,//for branches and jalr
	input [31:0] portRS2,//for branches (used in comparisons)
	input [2:0] funct3,//decide between beq/bne/blt/bge/bltu/bgeu; for jal and inc this is ignored, for jalr this should be 3'b000
	input [31:0] immediateJ,//jal immediate
	input [31:0] immediateI,//jalr immediate
	input [31:0] immediateB,//immediate type for branch instructions
	input [31:0] pcIn//address of current instruction
);
/* Helper Wires */
wire [31:0] nextInstruction = pcIn + 4;//the instruction in memory immedatly following this one (not necessarly the one sent to newPCOut)
wire [31:0] jumpRD = nextInstruction;//jal and jalr both put the address of the instruction after them into the rd register
wire [31:0] branchFailNewPC = nextInstruction;//if a branch condition is not satasfied then this is the new address of the pc (pc + 4)
wire [31:0] branchSucessNewPC = immediateB + pcIn;//if a branch condition is satasfied then this is the new address of the pc 

/* Output Logic */
always @*
begin
	if (outputEnable)
	begin
		case (operation)
			2'b00://jal
			begin
				newPCOut = immediateJ + pcIn;
				rdOut = jumpRD;
				invalidFunct3 = 1'b0;//funct3 is not present for jal
			end
			2'b01://jalr
			begin
				newPCOut = immediateI + portRS1;
				rdOut = jumpRD;
				invalidFunct3 = funct3 == 3'b000 ? 1'b0 : 1'b1;//funct3 should be 3'b000
			end
			2'b10://branch instructions
			begin
				case (funct3)//Todo implement myself for practice instead of relying on verilog operators
					3'b000: newPCOut = (portRS1 == portRS2) ? branchSucessNewPC : branchFailNewPC;//beq
					3'b001: newPCOut = (portRS1 != portRS2) ? branchSucessNewPC : branchFailNewPC;//bne
					3'b100: newPCOut = ($signed(portRS1) < $signed(portRS2)) ? branchSucessNewPC : branchFailNewPC;//blt
					3'b101: newPCOut = ($signed(portRS1) >= $signed(portRS2)) ? branchSucessNewPC : branchFailNewPC;//bge
					3'b110: newPCOut = (portRS1 < portRS2) ? branchSucessNewPC : branchFailNewPC;//bltu
					3'b111: newPCOut = (portRS1 >= portRS2) ? branchSucessNewPC : branchFailNewPC;//bgeu
					default: newPCOut = 32'h00000000;
				endcase
				
				rdOut = 32'h00000000;//Don't output because branch instructions do not change rd; control logic should not latch this
				
				if ((funct3 == 3'b010) || (funct3 == 3'b011))//invalid funct3
					invalidFunct3 = 1'b1;
				else
					invalidFunct3 = 1'b0;
			end
			2'b11://pc increment
			begin
				newPCOut = nextInstruction;
				rdOut = 32'h00000000;//no need to write to main registers
				invalidFunct3 = 1'b0;//not an instruction, so we don't have a funct3
			end
			default://will never happen
			begin
				newPCOut = 32'h00000000;
				rdOut = 32'h00000000;
				invalidFunct3 = 1'b0;
			end
		endcase
	end
	else
	begin
		newPCOut = 32'h00000000;
		rdOut = 32'h00000000;
		invalidFunct3 = 1'b0;
	end
end

assign jumpMisaligned = (newPCOut % 4) != 0;//new pc is not aligned to a 4 byte address

endmodule
