/* Bit extention functions (Sign and zero extention) */
//todo

function automatic [31:0] signExtend8To32(input [7:0] dataIn);
begin
	signExtend8To32 = {{24{dataIn[7]}}, dataIn};
end
endfunction

function automatic [31:0] zeroExtend8To32(input [7:0] dataIn);
begin
	zeroExtend8To32 = {24'h000000, dataIn};
end
endfunction

function automatic [31:0] signExtend16To32(input [15:0] dataIn);
begin
	signExtend16To32 = {{16{dataIn[5]}}, dataIn};
end
endfunction

function automatic [31:0] zeroExtend16To32(input [15:0] dataIn);
begin
	zeroExtend16To32 = {16'h0000, dataIn};
end
endfunction