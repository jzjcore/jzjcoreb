module ControlLogic
(
	input clock,
	input reset,
	
	//Control lines
	output reg registerFileWE,//Latch new data into register rd at next posedge
	//ALU
	output reg aluOE,//Output alu result to registerFileIn
	output reg opImm,//Whether the opcode type is opImm (1) or op (0)
	//Value former
	output reg valueFormerOE,//Output formed value to registerFileIn
	output reg valueFormerOperation,//Whether to output the value for a lui (0) is auipc (1) instruction
	//Program counter
	output reg pcWE,
	//Program counter ALU
	output reg pcALUOE,
	output reg [1:0] pcALUOperation,
	//Memory
	output reg memoryEnable,
	output reg [1:0] memoryOperation,
	
	//Error flags
	input pcMisaligned,//program counter is not aligned to a 4 byte address
	input jumpMisaligned,//new program counter address is not aligned to a 4 byte address
	input memoryAccessMisaligned,//memory access is not aligned to a 4 byte address for a word or a 2 byte address for a half word
	input badInstruction,//bad encoding (funct3, funct7)
	
	//Instruction stuffs
	input [31:0] instructionIn,//Instruction fetch from memory module
	output [31:7] instructionOutNoOpcode,//Instruction out (except for opcode) to be decoded by decoder module
	input [2:0] funct3//from decoder module
);
/* Wires, Registers, and Constants */

//State stuff
localparam STATE_IFETCH = 4'b0001;
localparam STATE_EXECUTEA = 4'b0010;
localparam STATE_EXECUTEB = 4'b0100;
localparam STATE_HALT = 4'b1000;

reg [3:0] currentState = STATE_IFETCH;
reg [3:0] nextState;
reg needsTwoCycles;//if the instruction needs to use the EXECUTEB state
reg halt = 1'b0;//next state should be STATE_HALT and the cpu should halt execution

//Other stuff
reg latchInstruction;//whether to latch instruction at the next negedge or not
reg [31:0] instructionRegister = 32'h00000000;//latched on negative edge of clock after memory instruction read operation
wire [6:0] opcode = instructionRegister[6:0];
assign instructionOutNoOpcode = instructionRegister[31:7];

/* State Logic */

//Change state each negedge
always @(negedge clock, posedge reset)
begin
	if (reset)
		currentState <= STATE_IFETCH;
	else if (~clock)
		currentState <= nextState;//current_state becomes new_state each clock pulse
	//else//This shouldn't be needed, but I am terrified of inferred latches so this is the anti-inferred-latches-warning superstisious object thing//todo figure out why this is needed
	//	currentState <= STATE_IFETCH;
end

//Decide next state
always @*
begin
	if (halt || pcMisaligned || jumpMisaligned || memoryAccessMisaligned || badInstruction)
		nextState = STATE_HALT;
	else//we're not halting the cpu
	begin
		case (currentState)
			STATE_IFETCH: nextState = STATE_EXECUTEA;//memory only takes 1 clock cycle to fetch an instruction
			STATE_EXECUTEA: nextState = (needsTwoCycles == 1) ? STATE_EXECUTEB : STATE_IFETCH;//case for instructions that take 2 clock cycles (sh, sb, load instructions)
			STATE_EXECUTEB: nextState = STATE_IFETCH;
			STATE_HALT: nextState = STATE_HALT;//spin forever
		endcase
	end
end

//Decide number of states needed based on the instruction
always @*
begin
	case (opcode)
		7'b0010011: needsTwoCycles = 1'b0;//alu immediate operation
		7'b0110011: needsTwoCycles = 1'b0;//alu register-register operation (from base spec or m extension)
		7'b0000011: needsTwoCycles = 1'b1;//memory read
		7'b0100011://memory write
		begin
			if (funct3 == 3'b010)
				needsTwoCycles = 1'b0;//sw
			else
				needsTwoCycles = 1'b1;//sb or sh because of read+modify-write cycle
		end
		7'b0110111: needsTwoCycles = 1'b0;//lui
		7'b0010111: needsTwoCycles = 1'b0;//auipc
		7'b0001111: needsTwoCycles = 1'b0;//fence/fence.i
		7'b1110011: needsTwoCycles = 1'b0;//ecall/ebreak
		7'b1101111: needsTwoCycles = 1'b0;//jal
		7'b1100111: needsTwoCycles = 1'b0;//jalr
		7'b1100011: needsTwoCycles = 1'b0;//branch instructions
		default needsTwoCycles = 1'b0;//if we have a bad opcode, we halt which only needs EXECUTEA
	endcase
end

/* Control Signal Logic */

always @*
begin
	case (currentState)
		STATE_IFETCH:
		begin
			//Reset things from last instruction
			registerFileWE = 1'b0;
			aluOE = 1'b0;
			opImm = 1'b0;
			valueFormerOE = 1'b0;
			valueFormerOperation = 1'b0;
			pcWE = 1'b0;
			pcALUOE = 1'b0;
			pcALUOperation = 2'b00;
			halt = 1'b0;
			
			//Get new instruction
			memoryEnable = 1'b1;//Enable memory
			memoryOperation = 2'b11;//Fetch instruction from memory
			latchInstruction = 1'b1;//Latch instruction on following negedge
		end
		STATE_EXECUTEA:
		begin
			case (opcode)
				7'b0010011://alu immediate operation
				begin
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					pcALUOE = 1'b0;
					pcALUOperation = 2'b00;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					latchInstruction = 1'b0;
					halt = 1'b0;
					
					aluOE = 1'b1;//Get result from alu
					opImm = 1'b1;//The opcode type is OP-IMM
					registerFileWE = 1'b1;//Store in rd
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b11;//increment pc (won't write to register file bus)
					pcWE = 1'b1;//store new pc
				end
				7'b0110011://alu register-register operation (from base spec or m extension)
				begin
					
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					latchInstruction = 1'b0;
					halt = 1'b0;
					
					aluOE = 1'b1;//Get result from alu
					opImm = 1'b0;//The opcode type is OP
					registerFileWE = 1'b1;//Store in rd
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b11;//increment pc (won't write to register file bus)
					pcWE = 1'b1;//store new pc
				end
				7'b0000011://memory read
				begin
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					latchInstruction = 1'b0;
					halt = 1'b0;
					
					registerFileWE = 1'b0;//can't latch until second posedge
					memoryEnable = 1'b1;//Enable memory
					memoryOperation = 2'b00;//load instruction
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b11;//increment pc (won't write to register file bus)
					pcWE = 1'b1;//store new pc
				end
				7'b0100011://memory write
				begin
					registerFileWE = 1'b0;
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					latchInstruction = 1'b0;
					halt = 1'b0;
					
					memoryEnable = 1'b1;//Enable memory
					
					if (funct3 == 3'b010)//sw instruction
						memoryOperation = 2'b10;//we don't care about the old data in the word because we are overwriting it completly on a single posedge
					else//sb or sh
						memoryOperation = 2'b01;//memory needs to read the old value stored at the address so it can modify it with a new byte/half word first to be written on the next posedge
				
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b11;//increment pc (won't write to register file bus)
					pcWE = 1'b1;//store new pc
				end
				7'b0110111://lui
				begin
					aluOE = 1'b0;
					opImm = 1'b0;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					latchInstruction = 1'b0;
					halt = 1'b0;
					
					registerFileWE = 1'b1;//Write upper immediate to rd
					valueFormerOE = 1'b1;//Get value from value former
					valueFormerOperation = 1'b0;//get lui value
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b11;//increment pc (won't write to register file bus)
					pcWE = 1'b1;//store new pc
				end
				7'b0010111://auipc
				begin
					aluOE = 1'b0;
					opImm = 1'b0;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					latchInstruction = 1'b0;
					halt = 1'b0;
					
					registerFileWE = 1'b1;//Write upper immediate to rd
					valueFormerOE = 1'b1;//Get value from value former
					valueFormerOperation = 1'b1;//get auipc value
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b11;//increment pc (won't write to register file bus)
					pcWE = 1'b1;//store new pc
				end
				7'b0001111://fence/fence.i
				begin
					//do nothing
					registerFileWE = 1'b0;
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					latchInstruction = 1'b0;
					halt = 1'b0;
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b11;//increment pc (won't write to register file bus)
					pcWE = 1'b1;//store new pc
				end
				7'b1110011://ecall/ebreak
				begin
					registerFileWE = 1'b0;
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					pcWE = 1'b0;
					pcALUOE = 1'b0;
					pcALUOperation = 2'b00;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					latchInstruction = 1'b0;
					
					halt = 1'b1;//ecalls/ebreaks just cause a fatal trap
				end
				7'b1101111://jal
				begin
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					latchInstruction = 1'b0;
					halt = 1'b0;
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b00;//jal
					registerFileWE = 1'b1;//Store new rd value
					pcWE = 1'b1;//Store new pc value
				end
				7'b1100111://jalr
				begin
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					latchInstruction = 1'b0;
					halt = 1'b0;
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b01;//jalr
					registerFileWE = 1'b1;//Store new rd value
					pcWE = 1'b1;//Store new pc value
				end
				7'b1100011://branch instructions			
				begin
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					latchInstruction = 1'b0;
					halt = 1'b0;
					
					pcALUOE = 1'b1;//Enable pc alu
					pcALUOperation = 2'b10;//branch instruction
					registerFileWE = 1'b0;//Branch instructions do not have an rd to store
					pcWE = 1'b1;//Store new pc value
				end
				default:
				begin
					//Disable everything
					registerFileWE = 1'b0;
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					pcWE = 1'b0;
					pcALUOE = 1'b0;
					pcALUOperation = 2'b00;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					latchInstruction = 1'b0;
					
					halt = 1'b1;//Bad opcode
				end
			endcase
		end
		STATE_EXECUTEB:
		begin
			case (opcode)
				7'b0000011://memory read
				begin
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					pcWE = 1'b0;
					pcALUOE = 1'b0;
					pcALUOperation = 2'b00;
					latchInstruction = 1'b0;
					halt = 1'b0;
					
					registerFileWE = 1'b1;//now we can latch the data into rd
					memoryEnable = 1'b1;//Leave memory enabled so it continues to output its value
					memoryOperation = 2'b00;//load instruction
				end
				7'b0100011://memory write
				begin
					registerFileWE = 1'b0;
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					pcWE = 1'b0;
					pcALUOE = 1'b0;
					pcALUOperation = 2'b00;
					latchInstruction = 1'b0;
					halt = 1'b0;
					
					memoryEnable = 1'b1;//Enable memory
					memoryOperation = 2'b10;//Now we can write the modified word back to memory
				end
				default://Any other opcode should not need 2 cycles
				begin
					//Disable everything
					registerFileWE = 1'b0;
					aluOE = 1'b0;
					opImm = 1'b0;
					valueFormerOE = 1'b0;
					valueFormerOperation = 1'b0;
					pcWE = 1'b0;
					pcALUOE = 1'b0;
					pcALUOperation = 2'b00;
					memoryEnable = 1'b0;
					memoryOperation = 2'b00;
					latchInstruction = 1'b0;
					
					halt = 1'b1;//Something went wrong
				end
			endcase
		end
		STATE_HALT:
		begin
			//Leave all outputs and registers as is
			registerFileWE = 1'b0;
			aluOE = 1'b0;
			opImm = 1'b0;
			valueFormerOE = 1'b0;
			valueFormerOperation = 1'b0;
			pcWE = 1'b0;
			pcALUOE = 1'b0;
			pcALUOperation = 2'b00;
			memoryEnable = 1'b0;
			memoryOperation = 2'b00;
			latchInstruction = 1'b0;
			
			halt = 1'b1;//There is no escaping the halt state, but leave this as 1 anyways
		end
	endcase
end

/* Instruction Register Latching Logic */

always @(negedge clock, posedge reset)
begin
	if (reset)
		instructionRegister <= 32'h00000000;
	else if (~clock)//negedge after memory read
	begin
		if (latchInstruction)
			instructionRegister <= instructionIn;
	end
end

/* Control Signal Initilization */

endmodule